﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Proj1GradVenkatesalu2.Pages
{
    public class TemperatureHelper
    {
        public static (bool passed, double convertedValue) F2C( string temp)
        {

            if (int.TryParse(temp, out int tempValue))
            {
                double convertedValue = ((tempValue - 32) * (5.0f / 9));
                return (true, convertedValue);
            }
            else
            {
                return (false, 0);
            }

        }

        public static (bool passed, double convertedValue) C2F(string temp)
        {

            if (int.TryParse(temp, out int tempValue))
            {
                double convertedValue = tempValue * ( 9.0f / 5 ) + 32;
                return (true, convertedValue);
            }
            else
            {
                return (false, 0);
            }

        }
    }
}
