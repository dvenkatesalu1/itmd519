﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Proj1GradVenkatesalu2.Pages
{
    public class IndexModel : PageModel
    {
        public string Message { get; private set; }

        [Required]
        [BindProperty]
        public string Temperature { get; set; }

        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            Message = "Hello ITMD 519!";
        }

        public void OnPost()
        {
            System.Console.WriteLine("The value is " + Temperature);

            (bool passed, double celc) = TemperatureHelper.F2C(Temperature);
            if(passed)
            { 
                Message = $"{Temperature} Fahrenheit is {Math.Round(celc, 0)} Celsius";
            }
            else
            { 
                Message = $"{Temperature} is not a valid number";
            }
        }
    }
}
